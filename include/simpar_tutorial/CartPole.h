/**********************************************************************************************************************
This file is part of the Control Toolbox (https://adrlab.bitbucket.io/ct), copyright by ETH Zurich, Google Inc.
Licensed under Apache2 license (see LICENSE file in main directory)
**********************************************************************************************************************/

#pragma once

#include <iostream>
#include <memory>

#include <ct/core/core.h>


namespace ct {
namespace models {

template <typename SCALAR>
class CartPole : public ct::core::ControlledSystem<4, 1, SCALAR>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    static const size_t nStates = 4;
    static const size_t nControls = 1;

    CartPole(
      std::shared_ptr<ct::core::Controller<nStates, nControls, SCALAR>> controller = nullptr,
      double m_cart = 1.5, double m_pendulum = 1.2, double l = 0.9) : 
        m_cart_(m_cart),
        m_pendulum_(m_pendulum),
        l_(l),
        ct::core::ControlledSystem<nStates, nControls, SCALAR>(controller)
    {
    }

    CartPole(const CartPole& arg) : 
      g_(arg.g_),
      m_cart_(arg.m_cart_),
      m_pendulum_(arg.m_pendulum_),
      l_(arg.l_),
      ct::core::ControlledSystem<nStates, nControls, SCALAR>(arg)
   {}

    virtual CartPole* clone() const override { return new CartPole(*this); }
    virtual void computeControlledDynamics(const ct::core::StateVector<nStates, SCALAR>& state,
        const SCALAR& t, // ignored since not time dependent
        const ct::core::ControlVector<nControls, SCALAR>& control,
        ct::core::StateVector<nStates, SCALAR>& derivative) override
    {
        // convenience accessors
        const SCALAR& x = state(0); // cart position
        const SCALAR& q = state(1); // pendulum angle
        const SCALAR& x_dot = state(2); // cart velocity
        const SCALAR& q_dot = state(3); // pendulum angular velocity

        SCALAR& dx = derivative(0);
        SCALAR& dq = derivative(1);

        // integrate velocities to position
        dx = x_dot;
        dq = q_dot;

        // M ddot{q}x`x`x` = F

        // M
        Eigen::Matrix<SCALAR, nStates/2, nStates/2> M;
        M << cos(q), l_,
             m_cart_ + m_pendulum_, m_pendulum_ * l_ * cos(q);

        Eigen::Matrix<SCALAR, nStates/2, 1> F;
        F << -g_ * sin(q), control(0) + m_pendulum_*l_*q_dot*sin(q);

        // ddot{q} = M^-1 F
        derivative.template tail<2>() = M.inverse() * F;
    }

    const SCALAR& m_cart() const { return m_cart_;}
    const SCALAR& m_pendulum() const { return m_pendulum_;}
    const SCALAR& l() const { return l_;}
private:
   const SCALAR g_ = SCALAR(9.81); // gravity constant
   SCALAR m_cart_; // mass of the cart
   SCALAR m_pendulum_; // mass of the pendulum
   SCALAR l_; // length of the pendulum
};

} // namespace models
} // namespace ct
