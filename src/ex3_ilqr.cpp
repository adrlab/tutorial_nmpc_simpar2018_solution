/*!
 * ex3_ilqr
 *
 */
#include <ct/optcon/optcon.h> // also includes ct-core
#include <simpar_tutorial/CartPole.h>
#include <simpar_tutorial/PlotHelper.h>


using namespace ct::core;
using namespace ct::optcon;
using ct::models::CartPole;

// get the state and control input dimension of the oscillator
const size_t state_dim = CartPole<double>::nStates;
const size_t control_dim = CartPole<double>::nControls;

ct::core::Time timeHorizon = 3.0;  // final time horizon in [sec]
double dt = 0.05; // time step

void test_iLQR(const ct::core::StateFeedbackController<state_dim, control_dim>& ilqr)
{
    std::shared_ptr<ct::core::StateFeedbackController<state_dim, control_dim>> controller(
        new ct::core::StateFeedbackController<state_dim, control_dim>(ilqr));

    // create an instance of the cart pole
    std::shared_ptr<ct::core::ControlledSystem<state_dim, control_dim, double>> cartpole(
        new CartPole<double>(controller));

    // create an integrator
    ct::core::Integrator<state_dim> integrator(cartpole, IntegrationType::EULERCT);

    // create a state
    ct::core::StateVector<state_dim> x;

    // we initialize it at 0
    x.setZero();

    // simulate
    ct::core::Time t0 = 0.0;
    size_t nSteps = timeHorizon/dt;

    // Plot helper
    PlotHelper helper;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i=0; i<nSteps; i++) {
    	integrator.integrate_n_steps(x, t0 + i*dt, 1, dt);
        helper.plot(x);
        std::this_thread::sleep_until(start + std::chrono::duration<double>(i*dt));
    }
}


int main(int argc, char **argv)
{
    /* STEP 1: set up the Nonlinear Optimal Control Problem
	 * First of all, we need to create instances of the system dynamics, the linearized system and the cost function. */

    /* STEP 1-A: create an instance of the cart pole */
    std::shared_ptr<ControlledSystem<state_dim, control_dim, double>> cartpole(
        new CartPole<double>());

    // create an auto-differentiable instance of the cart pole
    std::shared_ptr<ct::core::ControlledSystem<state_dim, control_dim, ct::core::ADCGScalar>> cartpoleAd(
        new CartPole<ct::core::ADCGScalar>());


    /* STEP 1-B: Although the first order derivatives of the cart pole are easy to derive, let's illustrate the use of the System Linearizer,
	 * which performs numerical differentiation by the finite-difference method. The system linearizer simply takes the
	 * the system dynamics as argument. Alternatively, you could implement your own first-order derivatives by overloading the class LinearSystem.h */
    std::shared_ptr<ADCodegenLinearizer<state_dim, control_dim>> adLinearizer(
        new ADCodegenLinearizer<state_dim, control_dim>(cartpoleAd));

    // compile the linearized model just-in-time
    adLinearizer->compileJIT();


    /* STEP 1-C: create a cost function. We have pre-specified the cost-function weights for this problem in "nlocCost.info".
	 * Here, we show how to create terms for intermediate and final cost and how to automatically load them from the configuration file.
	 * The verbose option allows to print information about the loaded terms on the terminal. */
    std::shared_ptr<TermQuadratic<state_dim, control_dim>> intermediateCost(
        new TermQuadratic<state_dim, control_dim>());
    std::shared_ptr<TermQuadratic<state_dim, control_dim>> finalCost(
        new TermQuadratic<state_dim, control_dim>());
    bool verbose = true;
    std::string exampleDir = "/home/adrl/catkin_ws/src/tutorial_nmpc_simpar2018_solution/src";
    intermediateCost->loadConfigFile(exampleDir + "/nlocCost.info", "intermediateCost", verbose);
    finalCost->loadConfigFile(exampleDir + "/nlocCost.info", "finalCost", verbose);

    // Since we are using quadratic cost function terms in this example, the first and second order derivatives are immediately known and we
    // define the cost function to be an "Analytical Cost Function". Let's create the corresponding object and add the previously loaded
    // intermediate and final term.
    std::shared_ptr<CostFunctionQuadratic<state_dim, control_dim>> costFunction(
        new CostFunctionAnalytical<state_dim, control_dim>());
    costFunction->addIntermediateTerm(intermediateCost);
    costFunction->addFinalTerm(finalCost);


    /* STEP 1-D: initialization with initial state and desired time horizon */
    StateVector<state_dim> x0;
    x0.setZero();  // Cart pole is hanging down

    // STEP 1-E: create and initialize an "optimal control problem"
    ContinuousOptConProblem<state_dim, control_dim> optConProblem(
        timeHorizon, x0, cartpole, costFunction, adLinearizer);


    /* STEP 2: set up a nonlinear optimal control solver. */

    /* STEP 2-A: Create the settings.
	 * the type of solver, and most parameters, like number of shooting intervals, etc.,
	 * can be chosen using the following settings struct. Let's use, the iterative
	 * linear quadratic regulator, iLQR, for this example. In the following, we
	 * modify only a few settings, for more detail, check out the NLOptConSettings class. */
    NLOptConSettings ilqr_settings;
    ilqr_settings.dt = dt;  // the control discretization in [sec]
    ilqr_settings.integrator = ct::core::IntegrationType::EULERCT;
    ilqr_settings.discretization = NLOptConSettings::APPROXIMATION::FORWARD_EULER;
    ilqr_settings.max_iterations = 100;
    ilqr_settings.nThreads = 1;  // use multi-threading
    ilqr_settings.nlocp_algorithm = NLOptConSettings::NLOCP_ALGORITHM::ILQR;
    ilqr_settings.lqocp_solver =
        NLOptConSettings::LQOCP_SOLVER::GNRICCATI_SOLVER;  // solve LQ-problems using custom Riccati solver
    ilqr_settings.printSummary = true;
    ilqr_settings.closedLoopShooting = true;
    ilqr_settings.min_cost_improvement = 1e-3;
    ilqr_settings.lineSearchSettings.active = true;


    /* STEP 2-B: provide an initial guess */
    // calculate the number of time steps K
    size_t K = ilqr_settings.computeK(timeHorizon);

    /* design trivial initial controller for iLQR. Note that in this simple example,
	 * we can simply use zero feedforward with zero feedback gains around the initial position.
	 * In more complex examples, a more elaborate initial guess may be required.*/
    FeedbackArray<state_dim, control_dim> u0_fb(K, FeedbackMatrix<state_dim, control_dim>::Zero());
    ControlVectorArray<control_dim> u0_ff(K, ControlVector<control_dim>::Zero());
    StateVectorArray<state_dim> x_ref_init(K + 1, x0);
    NLOptConSolver<state_dim, control_dim>::Policy_t initController(x_ref_init, u0_ff, u0_fb, ilqr_settings.dt);


    // STEP 2-C: create an NLOptConSolver instance
    NLOptConSolver<state_dim, control_dim> iLQR(optConProblem, ilqr_settings);

    // set the initial guess
    iLQR.setInitialGuess(initController);

    // STEP 3: solve the optimal control problem
    iLQR.solve();

    // STEP 4: retrieve the solution
    ct::core::StateFeedbackController<state_dim, control_dim> solution = iLQR.getSolution();

    std::cout<<"press any key to visualize";
    std::cin.ignore();

    test_iLQR(solution);
}
