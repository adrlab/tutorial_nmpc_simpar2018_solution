 /*!
 * Simple example how to linearize a system and design an LQR controller.
 *
 * ex2_lqr.cpp
 */


#include <ct/optcon/optcon.h>  // also includes ct_core
#include <simpar_tutorial/CartPole.h>
#include <simpar_tutorial/PlotHelper.h>

using ct::models::CartPole;

// get the state and control input dimension of the oscillator
const size_t state_dim = CartPole<double>::nStates;
const size_t control_dim = CartPole<double>::nControls;

void test_LQR(const ct::core::FeedbackMatrix<state_dim, control_dim>& K)
{
    std::shared_ptr<ct::core::ConstantStateFeedbackController<state_dim, control_dim>> controller(
        new ct::core::ConstantStateFeedbackController<state_dim, control_dim>());

    ct::core::ControlVector<control_dim> uff; uff.setZero();
    ct::core::StateVector<state_dim> x_ref; x_ref << 0.0, 3.14, 0.0, 0.0; // upright position
    controller->updateControlLaw(uff, x_ref, -K);

    // create an instance of the cart pole
    std::shared_ptr<ct::core::ControlledSystem<state_dim, control_dim, double>> cartpole(
        new CartPole<double>(controller));

    // create an integrator
    ct::core::Integrator<state_dim> integrator(cartpole);

    // create a state
    ct::core::StateVector<state_dim> x;

    // we initialize it at 0
    x.setZero();
    // except for an angle
    x(1) = 2.5; 

    // simulate
    double dt = 0.001;
    ct::core::Time t0 = 0.0;
    size_t nSteps = 5000; // simulate 3 seconds

    // Plot helper
    PlotHelper helper;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i=0; i<nSteps; i++) {
    	integrator.integrate_n_steps(x, t0, 1, dt);
    	// print the new state
    	// std::cout << "state after integration: " << x.transpose() << std::endl;
        helper.plot(x);
        std::this_thread::sleep_until(start + std::chrono::duration<double>(i*dt));
    }
}

int main(int argc, char** argv)
{


    // create an auto-differentiable instance of the cart pole
    std::shared_ptr<ct::core::ControlledSystem<state_dim, control_dim, ct::core::ADCGScalar>> cartpole(
        new CartPole<ct::core::ADCGScalar>());

    // create an Auto-Differentiation Linearizer with code generation on the quadrotor model
    ct::core::ADCodegenLinearizer<state_dim, control_dim> adLinearizer(cartpole);

    // compile the linearized model just-in-time
    adLinearizer.compileJIT();

    // define the linearization point around steady state
    ct::core::StateVector<state_dim> x;
    x.setZero();
    x(1) = 3.14;
    ct::core::ControlVector<control_dim> u;
    u.setZero();
    double t = 0.0;

    // compute the linearization around the nominal state using the Auto-Diff Linearizer
    auto A = adLinearizer.getDerivativeState(x, u, t);
    auto B = adLinearizer.getDerivativeControl(x, u, t);

    // load the weighting matrices
    std::string exampleDir = "/home/neunertm/catkin_ws/src/simpar_tutorial/src";
    ct::optcon::TermQuadratic<state_dim, control_dim> quadraticCost;
    quadraticCost.loadConfigFile(exampleDir + "/lqrCost.info", "termLQR");
    auto Q = quadraticCost.stateSecondDerivative(x, u, t);    // x, u and t can be arbitrary here
    auto R = quadraticCost.controlSecondDerivative(x, u, t);  // x, u and t can be arbitrary here

    // design the LQR controller
    ct::optcon::LQR<state_dim, control_dim> lqrSolver;
    ct::core::FeedbackMatrix<state_dim, control_dim> K;

    std::cout << "A: " << std::endl << A << std::endl << std::endl;
    std::cout << "B: " << std::endl << B << std::endl << std::endl;
    std::cout << "Q: " << std::endl << Q << std::endl << std::endl;
    std::cout << "R: " << std::endl << R << std::endl << std::endl;

    lqrSolver.compute(Q, R, A, B, K, true, true);

    std::cout << "LQR gain matrix:" << std::endl << K << std::endl;

    test_LQR(K);

    return 1;
}
