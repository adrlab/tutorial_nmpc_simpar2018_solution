/*!
 *
 * ex1_integration.cpp
 */

#include <ct/core/core.h>
#include <simpar_tutorial/CartPole.h>
#include <simpar_tutorial/PlotHelper.h>

using ct::models::CartPole;

int main(int argc, char** argv)
{
    // a damped oscillator has two states, position and velocity
    const size_t state_dim = CartPole<double>::nStates;  // = 4

    // create a state
    ct::core::StateVector<state_dim> x;

    // we initialize it at 0
    x.setZero();
    // except for a small angle
    x(1) = 3.0; 

    // create our cart pole instance
    std::shared_ptr<CartPole<double>> cartpole(new CartPole<double>());

    // create an integrator
    ct::core::Integrator<state_dim> integrator(cartpole);

    // simulate
    double dt = 0.001;
    ct::core::Time t0 = 0.0;
    size_t nSteps = 3000; // simulate 3 seconds

    // Plot helper
    PlotHelper helper;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i=0; i<nSteps; i++) {
    	integrator.integrate_n_steps(x, t0, 1, dt);
    	// print the new state
    	// std::cout << "state after integration: " << x.transpose() << std::endl;
        helper.plot(x);
        std::this_thread::sleep_until(start + std::chrono::duration<double>(i*dt));
    }

    return 0;
}
